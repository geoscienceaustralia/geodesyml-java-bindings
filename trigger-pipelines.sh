#!/usr/bin/env bash

set -euo pipefail

apiVersion=2.0
workspace=geoscienceaustralia

script="$(basename "${BASH_SOURCE[0]}")"
function usage {
    echo "Usage: $script <repositories>
    where
        repositories
            bitbucket repositories to be triggered, separated by spaces."
    exit 1
}

if [[ $# -eq 0 ]]; then
    echo "Empty bitbucket repository names."
    usage
fi

repositories=("$@")
for repo in "${repositories[@]}"; do
    data='{
        "target": {
            "ref_name": "master",
            "ref_type": "branch",
            "type": "pipeline_ref_target"
        }
    }'

    statusCode=$(curl -X POST -s \
        -w "%{http_code}" \
        -u "${BITBUCKET_USERNAME}:${BITBUCKET_APP_PASSWORD}" \
        -H "Content-Type: application/json" \
        -d "${data}" \
        "https://api.bitbucket.org/${apiVersion}/repositories/${workspace}/${repo}/pipelines/")

    if [[ $statusCode -ne 201 ]]; then
        echo "Failed to trigger repository $repo. Status Code: $statusCode"
        exit 2
    fi
done
